﻿using System;
using System.Collections.Generic;

namespace prog_ing_DostavaHrane.Models.Context
{
    public partial class AspNetUserTokens
    {
        public string UserId { get; set; }
        public string LoginProvider { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }

        public AspNetUsers User { get; set; }
    }
}
