﻿using System;
using System.Collections.Generic;

namespace prog_ing_DostavaHrane.Models.Context
{
    public partial class Hrana
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public int RestoranId { get; set; }
        public int TipId { get; set; }
        public string Slika { get; set; }
        public int? Glasovi { get; set; }
        public int Kolicina { get; set; }

        public Restoran Restoran { get; set; }
        public Tip Tip { get; set; }
    }
}
