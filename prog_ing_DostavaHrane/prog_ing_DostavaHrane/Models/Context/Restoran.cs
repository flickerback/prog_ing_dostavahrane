﻿using System;
using System.Collections.Generic;

namespace prog_ing_DostavaHrane.Models.Context
{
    public partial class Restoran
    {
        public Restoran()
        {
            Hrana = new HashSet<Hrana>();
        }

        public int Id { get; set; }
        public string Naziv { get; set; }
        public int MjestoId { get; set; }
        public string Adresa { get; set; }
        public int? Likes { get; set; }

        public Mjesto Mjesto { get; set; }
        public ICollection<Hrana> Hrana { get; set; }
    }
}
