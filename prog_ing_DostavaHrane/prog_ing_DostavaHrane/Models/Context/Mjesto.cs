﻿using System;
using System.Collections.Generic;

namespace prog_ing_DostavaHrane.Models.Context
{
    public partial class Mjesto
    {
        public Mjesto()
        {
            Restoran = new HashSet<Restoran>();
        }

        public int Id { get; set; }
        public string Naziv { get; set; }
        public string PostanskiBroj { get; set; }

        public ICollection<Restoran> Restoran { get; set; }
    }
}
