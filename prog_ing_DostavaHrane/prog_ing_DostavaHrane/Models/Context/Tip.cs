﻿using System;
using System.Collections.Generic;

namespace prog_ing_DostavaHrane.Models.Context
{
    public partial class Tip
    {
        public Tip()
        {
            Hrana = new HashSet<Hrana>();
        }

        public int Id { get; set; }
        public string Naziv { get; set; }

        public ICollection<Hrana> Hrana { get; set; }
    }
}
