﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using prog_ing_DostavaHrane.Models.Context;

namespace prog_ing_DostavaHrane.Controllers
{
    public class HranasController : Controller
    {
        private readonly DostavaHraneP31ATigriciContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly UserManager<IdentityUser> _userManager;

        public HranasController(DostavaHraneP31ATigriciContext context, IHostingEnvironment hostingEnvironment, UserManager<IdentityUser> userManager)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _userManager = userManager;
        }

        // GET: Hranas
        public async Task<IActionResult> Index()
        {
            var dostavaHraneP31ATigriciContext = _context.Hrana.Include(h => h.Restoran).Include(h => h.Tip);
            return View(await dostavaHraneP31ATigriciContext.ToListAsync());
        }
        [HttpPost]
        public ActionResult UvecajGlas(int? id)
        {
            var hrana = _context.Hrana.Where(p => p.Id == id).FirstOrDefault();
            if (hrana.Glasovi==null)
            {
                hrana.Glasovi = 0;
            }
            hrana.Glasovi++;
            _context.SaveChanges();
            return Json(hrana.Glasovi);
        }
        public ActionResult DodajJos(int? id)
        {
            var hrana = _context.Hrana.Where(p => p.Id == id).FirstOrDefault();
            hrana.Kolicina++;
            _context.SaveChanges();
            return Json(hrana.Kolicina);
        }
        public ActionResult MakniJos(int? id)
        {
            var hrana = _context.Hrana.Where(p => p.Id == id).FirstOrDefault();
            if (hrana.Kolicina>1)
            {
                hrana.Kolicina--;
                _context.SaveChanges();
            }
            return Json(hrana.Kolicina);
        }
        public async Task<IActionResult> HranaPoRestoranu(int id)
        {
            var dostavaHraneP31ATigriciContext = _context.Hrana.Include(h => h.Restoran).Include(h => h.Tip).Where(h=>h.RestoranId==id);
            return View(await dostavaHraneP31ATigriciContext.ToListAsync());
        }
        public async Task<IActionResult> HranaPoKategoriji(int id)
        {
            var dostavaHraneP31ATigriciContext = _context.Hrana.Include(h => h.Restoran).Include(h => h.Tip).Where(h => h.TipId == id);
            return View(await dostavaHraneP31ATigriciContext.ToListAsync());
        }
        public async Task<IActionResult> TopHrana()
        {
            var dostavaHraneP31ATigriciContext = _context.Hrana.Include(h => h.Restoran).Include(h => h.Tip).OrderByDescending(p => p.Glasovi);
            return View(await dostavaHraneP31ATigriciContext.ToListAsync());
        }

        // GET: Hranas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hrana = await _context.Hrana
                .Include(h => h.Restoran)
                .Include(h => h.Tip)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (hrana == null)
            {
                return NotFound();
            }

            return View(hrana);
        }

        // GET: Hranas/Create
        public IActionResult Create()
        {
            ViewData["RestoranId"] = new SelectList(_context.Restoran, "Id", "Adresa");
            ViewData["TipId"] = new SelectList(_context.Tip, "Id", "Naziv");
            return View();
        }

        // POST: Hranas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Naziv,RestoranId,TipId,Slika")] Hrana hrana)
        {
            if (ModelState.IsValid)
            {
                _context.Add(hrana);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["RestoranId"] = new SelectList(_context.Restoran, "Id", "Adresa", hrana.RestoranId);
            ViewData["TipId"] = new SelectList(_context.Tip, "Id", "Naziv", hrana.TipId);
            return View(hrana);
        }

        // GET: Hranas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hrana = await _context.Hrana.FindAsync(id);
            if (hrana == null)
            {
                return NotFound();
            }
            ViewData["RestoranId"] = new SelectList(_context.Restoran, "Id", "Adresa", hrana.RestoranId);
            ViewData["TipId"] = new SelectList(_context.Tip, "Id", "Naziv", hrana.TipId);
            return View(hrana);
        }

        // POST: Hranas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Naziv,RestoranId,TipId,Slika")] Hrana hrana)
        {
            if (id != hrana.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(hrana);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HranaExists(hrana.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["RestoranId"] = new SelectList(_context.Restoran, "Id", "Adresa", hrana.RestoranId);
            ViewData["TipId"] = new SelectList(_context.Tip, "Id", "Naziv", hrana.TipId);
            return View(hrana);
        }

        // GET: Hranas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hrana = await _context.Hrana
                .Include(h => h.Restoran)
                .Include(h => h.Tip)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (hrana == null)
            {
                return NotFound();
            }

            return View(hrana);
        }
        public IActionResult Narudzba(int id)
        {
            // Budući da spremamo listu ovo je prihvatljivo rješenje:
            List<int> narudzbe = new List<int>();
            narudzbe.Add(id);
            string serijalizirana_lista = JsonConvert.SerializeObject(narudzbe);

            // FINALNO rješenje za upis u Session varijablu Posudbe:
            string narudzbe_string = HttpContext.Session.GetString("Narudzba");
            if (narudzbe_string == null)
            {
                narudzbe = new List<int>();
            }
            else
            {
                narudzbe = JsonConvert.DeserializeObject<List<int>>(narudzbe_string);
            }
            if (!narudzbe.Contains(id))
            {
                narudzbe.Add(id);
            }
            var temp = JsonConvert.SerializeObject(narudzbe);
            HttpContext.Session.SetString("Narudzba", temp);

            // Dohvat hrane u kosarici
            var hrana = _context.Hrana
                .Include(h => h.Restoran)
                .Include(h => h.Tip)
                .Where(item => narudzbe.Contains(item.Id));
            return View(hrana.ToList());
        }
        public IActionResult DeleteIzKosarice(int id)
        {
            List<int> narudzbe = null;
            string narudzbe_string = HttpContext.Session.GetString("Narudzba");
            if (narudzbe_string == null)
            {
                narudzbe = new List<int>();
            }
            else
            {
                narudzbe = JsonConvert.DeserializeObject<List<int>>(narudzbe_string);
            }
            if (narudzbe.Contains(id))
            {
                narudzbe.Remove(id);
                var hran=_context.Hrana.Where(item => item.Id==id);
                foreach (var item in hran)
                {
                    item.Kolicina = 1;
                }
                _context.SaveChanges();
            }
            var temp = JsonConvert.SerializeObject(narudzbe);
            HttpContext.Session.SetString("Narudzba", temp);

            // Dohvat hrane u kosarici
            var hrana = _context.Hrana.Where(item => narudzbe.Contains(item.Id));

            return View("Narudzba", hrana.ToList());
        }
        public async Task<IActionResult> PotvrdiNarudzbu()
        {
            string narudzbe_string = HttpContext.Session.GetString("Narudzba");
            List<int> narudzbe = new List<int>();
            if (narudzbe_string != null)
            {
                narudzbe = JsonConvert.DeserializeObject<List<int>>(narudzbe_string);
            }

            // Dohvat hrane u kosarici
            var hrana = _context.Hrana
                .Include(h => h.Restoran)
                .Include(h => h.Tip)
                .Where(item => narudzbe.Contains(item.Id));

            var user = await _userManager.GetUserAsync(HttpContext.User);

            var smtpClient = new SmtpClient
            {
                Host = "smtp.gmail.com", // set your SMTP server name here
                Port = 587, // Port 
                EnableSsl = true,
                Credentials = new NetworkCredential("dostavahraneP31A@gmail.com", "ProjektPass2212")
            };

            string superstring = "Vaša narudžba: \n";
            using (StreamWriter writer = new StreamWriter("orders/order.txt", false))
            {
                int i = 0;
                writer.WriteLine("Vaša narudžba:");
                if (user!=null)
                {
                    writer.WriteLine(user.Email);
                }
                writer.WriteLine(" ");
                superstring+="\n";
                foreach (var item in hrana)
                {
                    i++;
                    writer.WriteLine("Stavka " + i + ": " + item.Naziv.TrimEnd()+" x "+item.Kolicina);
                    superstring +="Stavka " + i + ": " + item.Naziv.TrimEnd() + " x " + item.Kolicina+"\n";

                    item.Kolicina = 1;
                }
            }
            if (user!=null)
            {
                using (var message = new MailMessage("dostavahraneP31A@gmail.com", user.Email)
                {
                    Subject = "Narudžba.hr",
                    Body = superstring
                })
                {
                    await smtpClient.SendMailAsync(message);
                }
            }

            HttpContext.Session.Remove("Narudzba");

            _context.SaveChanges();
            return View();
        }

        // POST: Hranas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var hrana = await _context.Hrana.FindAsync(id);
            _context.Hrana.Remove(hrana);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HranaExists(int id)
        {
            return _context.Hrana.Any(e => e.Id == id);
        }
    }
}
