﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using prog_ing_DostavaHrane.Models.Context;

namespace prog_ing_DostavaHrane.Controllers
{
    public class RestoransController : Controller
    {
        private readonly DostavaHraneP31ATigriciContext _context;

        public RestoransController(DostavaHraneP31ATigriciContext context)
        {
            _context = context;
        }

        // GET: Restorans
        public async Task<IActionResult> Index()
        {
            var dostavaHraneP31ATigriciContext = _context.Restoran.Include(r => r.Mjesto);
            return View(await dostavaHraneP31ATigriciContext.ToListAsync());
        }
        public async Task<IActionResult> RestoraniPoGradu(int id)
        {
            var dostavaHraneP31ATigriciContext = _context.Restoran.Include(r => r.Mjesto).Where(r=>r.MjestoId==id);
            return View(await dostavaHraneP31ATigriciContext.ToListAsync());
        }

        // GET: Restorans/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restoran = await _context.Restoran
                .Include(r => r.Mjesto)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (restoran == null)
            {
                return NotFound();
            }

            return View(restoran);
        }

        // GET: Restorans/Create
        public IActionResult Create()
        {
            ViewData["MjestoId"] = new SelectList(_context.Mjesto, "Id", "Naziv");
            return View();
        }

        // POST: Restorans/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Naziv,MjestoId,Adresa")] Restoran restoran)
        {
            if (ModelState.IsValid)
            {
                _context.Add(restoran);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["MjestoId"] = new SelectList(_context.Mjesto, "Id", "Naziv", restoran.MjestoId);
            return View(restoran);
        }

        // GET: Restorans/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restoran = await _context.Restoran.FindAsync(id);
            if (restoran == null)
            {
                return NotFound();
            }
            ViewData["MjestoId"] = new SelectList(_context.Mjesto, "Id", "Naziv", restoran.MjestoId);
            return View(restoran);
        }

        // POST: Restorans/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Naziv,MjestoId,Adresa")] Restoran restoran)
        {
            if (id != restoran.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(restoran);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RestoranExists(restoran.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["MjestoId"] = new SelectList(_context.Mjesto, "Id", "Naziv", restoran.MjestoId);
            return View(restoran);
        }

        // GET: Restorans/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var restoran = await _context.Restoran
                .Include(r => r.Mjesto)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (restoran == null)
            {
                return NotFound();
            }

            return View(restoran);
        }

        // POST: Restorans/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var restoran = await _context.Restoran.FindAsync(id);
            _context.Restoran.Remove(restoran);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RestoranExists(int id)
        {
            return _context.Restoran.Any(e => e.Id == id);
        }
    }
}
